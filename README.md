# FuzzyART

<!-- badges: start -->
<!-- badges: end -->

The goal of FuzzyART is to train a cluster partitioning and to determine/predict cluster labels according to the FuzzyART algorithm intoduced in [[1]](#1).

## Installation

You can install the released version of FuzzyART from GitLab with:


``` r
devtools::install_gitlab(repo = "acil-group/rFuzzyART", host = "git.mst.edu")
```

Or from CRAN, once available, with:
``` r
install.packages("FuzzyART")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(FuzzyART)

# load data
inputs = subset(iris,select = -Species)
labels.true = as.numeric(unlist(iris$Species))
normalized_inputs = normalize(df = inputs)
# train model
mod  = FuzzyART_train(normalized_inputs,alpha = .8,rho = .5,
                      beta = .12, max_epochs = 1000,max_clusters =20, 
                      random_seed = 4, show_status = FALSE, beta_decay = .9)
plot(inputs, col = mod$Labels, pch = labels.true, 
     main = paste0("Dataset: Iris -- Rand Index: ", 
                   round(adjustedRandIndex(mod$Labels,labels.true),digits = 2)))
```
For further details and examples please visit the [vignette](http://dx.doi.org/10.13140/RG.2.2.11823.25761) [[2]](#2). When using this package please consider citing the vignette.
## References
<a id="1">[1]</a> 
Carpenter, Gail A., Stephen Grossberg, and David B. Rosen. 1991. “Fuzzy ART: Fast stable learning and
categorization of analog patterns by an adaptive resonance system.” Neural Networks 4 (6): 759–71.
https://doi.org/10.1016/0893-6080(91)90056-B.

<a id="2">[2]</a> 
L. Steinmeister and D. C. Wunsch II, “FuzzyART: An R Package for ART-based Clustering.,” 2021. http://dx.doi.org/10.13140/RG.2.2.11823.25761
