
train_pattern = function(pattern, w, alpha, rho, beta, num_clusters, max_cluster_count,
                         match_fn=match_fn_default, update_fn = update_fn_default,
                         distance_fn = fuzzy_and, vigilance_fn = vigilance_fn_default,
                         out.lst = list(w = NA, num_clusters=NA, winner = NA))
{
  #find the winmning category
  winner = eval_pattern(pattern = pattern,w = w,alpha = alpha, rho = rho,match_fn = match_fn, vigilance_fn= vigilance_fn)

  #did an uncommited category win? -> create a new category
  if(winner > num_clusters)
  {
    #create a new category
    num_clusters = num_clusters+1
    # was the maximum cluster count reached?
    if(num_clusters>max_cluster_count) stop("Error in 'train_pattern'. The maximum cluster count was exceeded!")
    w = rbind(w,pattern)#appendrep(1,dim(w)[2])
  }
  #overwrite old weights to include the initialization of the new one
  out.lst$w_old = w
  #update weight of winning category
  w[winner,] <-weight_update(pattern,w[winner,],beta,
                             update_fn = update_fn, distance_fn = distance_fn)

  out.lst$winner = winner
  out.lst$w = w
  out.lst$num_clusters = num_clusters
  return(out.lst)
}

eval_pattern = function(pattern,w, alpha, rho,
                        match_fn=match_fn_default, vigilance_fn = vigilance_fn_default)
{
  #w
  m = dim(w)[1]# number of clusters
  matches = rep(0,m)

  matches = apply(X = w,MARGIN = 1,FUN = function(weight){return(category_choice(pattern,weight,alpha, match_fn = match_fn))})
  #for(j in 1:m)
  #{
  #  matches[j]= category_choice(pattern,w[j,],alpha)
  #}
  match_attempts = 0
  while(match_attempts<length(matches))
  {
    #select the winning category
    winner = which.max(matches)
    if(vigilance_check(pattern, w[winner,],rho,vigilance_fn = vigilance_fn)) return(winner)
    else
    {
      #try next best category if vigilance test was failed
      matches[winner]<-0
      match_attempts<-match_attempts+1
    }
  }
  return(length(matches)+1)
}
pattern_compare = function(pattern, category_w,
                           distance_fn = fuzzy_and)
{
  return(distance_fn(pattern, category_w))
}
category_choice = function(pattern, category_w, alpha,
                           match_fn = match_fn_default)
{
  match_fn(pattern,category_w,alpha)
}
vigilance_check = function(pattern, category_w, rho,
                           vigilance_fn = vigilance_fn_default_one)
{
  return(vigilance_fn(pattern,category_w,rho))
}
weight_update = function(pattern,category_w,beta,
                         update_fn=update_fn_default,distance_fn=fuzzy_and)
{
  return(update_fn(pattern,category_w,beta,distance_fn))
}
plot.FuzzyART = function(object,...)
{
  plot(object$params$data,col = object$Labels, pch = object$Labels,...)
}
summary.FuzzyART = function(object){
  print(paste0("The training concluded after ", object$iterations, " Iterations"))
  print(paste0("A total number of ", length(unique(object$Label)), " classes were identified."))
  summary(object$Label)
}
#' Predict cluster labels based on a trained fuzzy ART model.
#'
#' @param object A trained fuzzy ART model of class FuzzyART.
#' @param ... Additional parameters such as the data for which a prediction is desired.
#'
#' @return Returns the labels for the specified observations.
#' @export
predict.FuzzyART = function(object, ...){
  FuzzyART_predict(FuzzyArt_Module = object,...)
}
